package service;

import io.quarkus.panache.common.Parameters;
import models.Color;
import repository.ColorRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

@ApplicationScoped
public class ColorService {

    @Inject
    ColorRepository colorRepository;

    @Transactional
    public boolean save(Color color){
        updateColor();
        color.setActivo(true);
        colorRepository.persist(color);
        return colorRepository.isPersistent(color);
    }

    @Transactional
    public int updateColor(){
        Color colorActivo = colorRepository.getColorActive();
        return colorRepository.update("activo=:activo WHERE id_color=:id_color",
                Parameters.with("activo", false).and("id_color", colorActivo.getId_color()));
    }

    public Color getColorActive(){
        return colorRepository.getColorActive();
    }
}
