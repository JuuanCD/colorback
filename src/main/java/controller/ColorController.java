package controller;

import models.Color;
import repository.ColorRepository;
import service.ColorService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@RequestScoped
@Path("color")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ColorController {
    @Inject
    private ColorService colorService;

    @GET
    @Path("/getColor")
    public Color getColor(){
        return colorService.getColorActive();
    }

    @POST
    @Path("/save")
    public boolean saveColor(Color color){
        return colorService.save(color);
    }

}
