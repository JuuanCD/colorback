package repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import models.Color;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@ApplicationScoped
public class ColorRepository implements PanacheRepository<Color> {

    @Inject
    EntityManager entityManager;

    public Color getColorActive(){
        return entityManager.createQuery("FROM color WHERE activo=true", Color.class).getSingleResult();
    }
}
