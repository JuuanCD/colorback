package models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity(name ="color")
public class Color {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "colorSequence")
    @SequenceGenerator(name="colorSequence", sequenceName = "colorSequence", allocationSize = 1, initialValue = 2)
    @JsonProperty(value = "id_color")
    private Long id_color;

    @JsonProperty(value = "primario")
    private String primario;

    @JsonProperty(value = "secundario")
    private String secundario;

    @JsonProperty(value = "terciario")
    private String terciario;

    @JsonProperty(value = "activo")
    private boolean activo;

    public String getPrimario() {
        return primario;
    }

    public Color() {
    }

    public Color(Long id_color, String primario, String secundario, String terciario, boolean activo) {
        this.id_color = id_color;
        this.primario = primario;
        this.secundario = secundario;
        this.terciario = terciario;
        this.activo = activo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getTerciario() {
        return terciario;
    }

    public void setTerciario(String terciario) {
        this.terciario = terciario;
    }

    public void setPrimario(String primario) {
        this.primario = primario;
    }

    public Long getId_color() {
        return id_color;
    }

    public void setId_color(Long id_color) {
        this.id_color = id_color;
    }

    public String getSecundario() {
        return secundario;
    }

    public void setSecundario(String secundario) {
        this.secundario = secundario;
    }
}
